# Angular Katas

Este repositorio contiene ejercicios que te ayudarán a mejorar tu habilidad 
con Angular y mejorar tu experiencia con el framework en proyectos reales.

Cada ejercicio tiene asociado un nivel de dificultad que puedes tomar como 
referencia.

## Katas

Los ejercicios disponibles actualmente agrupados por dificultad son los siguientes:

### Básico

* [Filtro de mayúsculas](katas/upper-case-first)
* [Registrador de clics](katas/on-click-logger)
* [Validador de DNI](katas/dni-validator)

### Medio

### Avanzado