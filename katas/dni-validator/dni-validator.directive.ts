import { forwardRef, Directive } from '@angular/core';
import { NG_VALIDATORS, Validator } from '@angular/forms';

export const DNI_VALIDATOR: any = {
  provide: NG_VALIDATORS,
  useExisting: forwardRef(() => DniValidatorDirective),
  multi: true
};

@Directive({
  selector: '[dni][formControlName],[dni][formControl],[dni][ngModel]',
  providers: [ DNI_VALIDATOR ]
})
export class DniValidatorDirective implements Validator {
  
}