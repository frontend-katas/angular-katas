# DNI Validation Directive

El módulo de registro de clientes de la aplicación necesita que el DNI que el usuario introduce
sea validado en el navegador antes de llegar al servidor.

La función para validar un DNI está ya desarrollada en la aplicación, así que solo tienes que crear
una directiva que implemente la interfaz `Validator` que aplicada sobre un campo de formulario valide
que su valor sea un DNI correcto.

> Si el valor del campo no es un DNI válido la directiva debe añadir el error `{ dni: true }`

```javascript
isValidDNI (dni: string): boolean {
  const DNI_REGEX = /^(\d{8})([A-Z])$/;
  
  if (dni && typeof dni === 'string' && this.DNI_REGEX.test(dni)) {
    let letter = this.DNI_LETTERS.charAt( parseInt( dni, 10 ) % 23 );

    return letter === dni.charAt(8);
  } else {
    return false;
  }
}
```