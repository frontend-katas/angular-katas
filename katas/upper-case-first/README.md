# Pipe

En tu aplicación se requiere que los nombres y apellidos de los clientes que aparezcan en ella
lo hagan siempre con la primera letra en mayúsculas. Implementa un **pipe** que reciba como parámetro
un *string* y devuelva **todas** las palabras de la cadena con la primera letra en mayúsculas y las demás
letras en minúsculas.

## Entrada / Salida

