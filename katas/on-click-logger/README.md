# On-click Logger Directive

El responsable de tu proyecto necesita para el entorno de desarrollo que cada vez que el usuario
haga clic en determinados elementos de la aplicación se muestre por consola un mensaje indicando
que ha hecho clic y el identificador del elemento sobre el que se ha ejecutado la acción.

Tu tarea es implementar una directiva que, aplicada sobre cualquier elemento HTML, realice lo que tu responsable necesita.

> En caso de que el elemento sobre el que se coloque la directiva no tenga un identificador deberás lanzar un error para avisar al desarrollador
